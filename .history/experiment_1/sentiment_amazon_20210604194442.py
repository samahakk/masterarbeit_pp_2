from flair.data import Corpus
from flair.datasets import ColumnCorpus
from flair.models.text_classification_model import TARSClassifier
from flair.trainers import ModelTrainer
from flair.datasets import SentenceDataset

import pandas as pd
import numpy as np

path = '/Users/hakkarsama/Documents/VPProjects/Masterarbeit_datasets/sentiment_amazon/tars_sample'

column_name_map = {0:'label', 1:'text'}

corpus: Corpus = ColumnCorpus(
       dev_file=path,
       train_file=None, 
       val_file=None,
       column_name_map,
       skip_first_line=True,
       column_delimiter=','
                            )

tars = TARSClassifier(task_name='sentiment_amazon')

trainer = ModelTrainer(tars, corpus)

trainer.train(base_path='resources/taggers/sentiment_amazon',
learning_rate=0.02, 
mini_batch_size=16,
mini_batch_chunk_size=4,
max_epochs=10
)