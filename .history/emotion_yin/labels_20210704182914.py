oneword = [
'joy',         
'surprise',
'love',        
'disgust',     
'fear',        
'anger',       
'guilt',       
'noemo',       
'shame',       
'sadness'     
]

wordnet_labels = {
'joy': 'the emotion of great happiness',         
'surprise': 'the astonishment you feel when something totally unexpected happens to you',
'love': 'a strong positive emotion of regard and affection',        
'disgust':'strong feelings of dislike',     
'fear':'an emotion experienced in anticipation of some specific pain or danger',        
'anger':'a feeling that is oriented toward some real or supposed grievance',       
'guilt': 'remorse caused by feeling responsible for some offense',       
'shame': 'a painful emotion resulting from an awareness of inadequacy or guilt',       
'sadness': 'emotions experienced when not in a state of well-being' 
}  

interpretation_labels = {
'joy':'this text expresses joy',         
'surprise':'this text expresses surprise',
'love':'this text expresses love',        
'disgust':'this text expresses disgust',     
'fear':'this text expresses fear',        
'anger':'this text expresses anger',       
'guilt':'this text expresses guilt',              
'shame':'this text expresses shame',       
'sadness':'this text expresses sadness' 
}
