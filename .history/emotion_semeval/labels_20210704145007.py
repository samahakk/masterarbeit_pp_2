oneword = [
       'sadness',     
       'optimism',    
       'anger',      
       'joy',    
       ]

wordnet_labels = {
       'sadness': 'emotions experienced when not in a state of well-being',
       'optimism': 'the optimistic feeling that all is going to turn out well',
       'anger': 'the state of being angry',
       'joy': 'the emotion of great happiness'
}


interpretation_labels = {
'sadness':'this text expresses sadness',     
'optimism':'this text expresses optimism',    
'anger':'this text expresses anger',      
'joy':'this text expresses joy'         
}