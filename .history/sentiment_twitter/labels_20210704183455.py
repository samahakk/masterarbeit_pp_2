wordnet_labels = {
    'negative': 'having the quality of something harmful or unpleasant',
    'neutral':'one who does not side with any party in a war or dispute',
    'positive': 'involving advantage or good'
}