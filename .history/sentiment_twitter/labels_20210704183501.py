wordnet_labels = {
    'negative': 'having the quality of something harmful or unpleasant',
    'neutral':'one who does not side with any party in a war or dispute',
    'positive': 'involving advantage or good'
}

interpretation_labels = {
    'negative': 'This text entails a negative sentiment',
    'neutral': 'This text entails a neutral sentiment',
    'positive': 'This text entails a positive sentiment'
}