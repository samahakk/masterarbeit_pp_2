wordnet_labels = {
"awful": "exceptionally bad or displeasing",
"bad": "that which is below standard or expectations as of ethics or decency",
"neutral": "one who does not side with any party in a war or dispute",
"good": "that which is pleasing or valuable or useful",
"great": "remarkable or out of the ordinary in degree or magnitude or effect",
}

interpretation_labels = {   
    'awful': 'The product has been reviewed as awful',
    'bad': 'The product has been reviewed as bad',
    'neutral': 'The product has been reviewed as neutral',
    'good': 'The product has been reviewed as good',
    'great': 'The product has been reviewed as great'}