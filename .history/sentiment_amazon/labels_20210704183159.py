wordnet_labels = {
"awful": "exceptionally bad or displeasing",
"bad": "that which is below standard or expectations as of ethics or decency",
"neutral": "one who does not side with any party in a war or dispute",
"good": "that which is pleasing or valuable or useful",
"great": "remarkable or out of the ordinary in degree or magnitude or effect",
}