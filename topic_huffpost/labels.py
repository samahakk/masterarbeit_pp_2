wordnet_labels = {
       'travel': 'undertake a journey or trip',
       'technology':'the practical application of science to commerce or industry',
       'wellness':'a healthy state of wellbeing free from disease',
       'women': 'an adult female person (as opposed to a man)',
       'parents': 'a father or mother; one who begets or one who gives birth to or nurtures and raises a child a relative who plays the role of guardian',
       'business': 'a father or mother; one who begets or one who gives birth to or nurtures and raises a child; a relative who plays the role of guardian',
       'weddings': 'the social event at which the ceremony of marriage is performed',
       'fashion': 'the latest and most admired style in clothes and cosmetics and behavior',
       'entertainment':'an activity that is diverting and that holds the attention',
       'science': 'a particular branch of scientific knowledge',
       'divorce': 'the legal dissolution of a marriage',
       'crime': 'an act punishable by law; usually considered an evil act an evil act not necessarily punishable by law',
       'religion': 'a strong belief in a supernatural power or powers that control human destiny',
       'sports': 'an active diversion requiring physical exertion and competition',
       'politics': 'social relations involving intrigue to gain authority or power',
       'comedy': 'light and humorous drama with a happy ending'
}


interpretation_labels = {
       'travel': 'this text is about travel',      
       'technology':        'this text is about technology',
       'wellness':          'this text is about wellness',  
       'women':             'this text is about women',     
       'parents':           'this text is about parents',   
       'business':          'this text is about business',   
       'weddings':          'this text is about weddings', 
       'fashion':           'this text is about fashion',   
       'entertainment':     'this text is about entertainmen,the attention',
       'science':           'this text is about science',   
       'divorce':           'this text is about divorce',  
       'crime':             'this text is about crime',   
       'religion':          'this text is about religion',
       'sports':            'this text is about sports',  
       'politics':          'this text is about politics', 
       'comedy':            'this text is about comedy'  
}

huffpost_dict = {
'TRAVEL': 'travel',         
'TECH': 'technology',             
'WELLNESS': 'wellness',              
'WOMEN': 'women',            
'PARENTS': 'parents',          
'BUSINESS': 'business',        
'WEDDINGS': 'weddings',         
'STYLE': 'fashion',            
'ENTERTAINMENT': 'entertainment',    
'SCIENCE': 'science',        
'DIVORCE': 'divorce',          
'CRIME': 'crime',            
'RELIGION': 'religion',        
'SPORTS': 'sports',           
'POLITICS': 'politics',         
'COMEDY': 'comedy'
}